require('es6-promise').polyfill();
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify');
    concat = require('gulp-concat');
gulp.task('default', ['watch'], function() {

});
gulp.task('watch', function() {
    gulp.watch('css/sass/*.scss', ['styles']);
});
gulp.task('js', function() {
    gulp.src(['./js/app/*.js','./js/app/controllers/*.js','./js/app/api/*.js'])
        .pipe(concat('script.js'))
        .pipe(gulp.dest('./js/'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('js'))

});
gulp.task('styles', function() {
    return sass('css/sass/', {style: "expanded"})
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1'))
        .pipe(gulp.dest('css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest('css'));
});
