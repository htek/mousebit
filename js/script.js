/**
 * Created by Tidus on 9/12/14.
 */
'use strict';
var app = angular.module('Mousebit', [
        "ui.router",
	    'Controllers',
        "ui.bootstrap",
        "ngResource",
        'ngRoute',
        "ngAnimate",
	    "ngCookies",
        "ngSanitize"
    ])
 .run ( ['$rootScope','$location', '$routeParams', function($rootScope, $location, $routeParams) {

	$rootScope.$on('$routeChangeSuccess', function(e, next, cur) {
	console.log(cur);
       if ($location.path() == '/' && typeof(cur) !== 'undefined') {
		console.log("what");
		window.location.href = '/';
       }
    });
}])

 .config( ['$routeProvider', '$locationProvider', '$resourceProvider'
,        function ($routeProvider,  $locationProvider, $resourceProvider) {
            $routeProvider.when('/login', {title: 'login',templateUrl: '/js/app/views/login/index.html', controller: 'LoginController'});
            $routeProvider.when('/new-quote', {title: 'new-quote',templateUrl: '/js/app/views/quotes/index.html', controller: 'QuotesController'});
            $routeProvider.when('/about-us', {title: 'About Us',templateUrl: '/js/app/views/aboutUs/index.html'});
            $routeProvider.when('/portfolios', {title: 'Portfolios',templateUrl: '/js/app/views/portfolio/index.html', controller: 'PortfoliosController'});
	        $routeProvider.when('/services/:service', {title: 'Services',templateUrl: '/js/app/views/services/index.html', controller: 'ServicesController'});
            $routeProvider.when('/payment', {title: 'Payment',templateUrl: '/js/app/views/payment/index.html', controller: 'PaymentController'});
		$routeProvider.when('/privacy-policy', {title: 'Privacy Policy',templateUrl: '/js/app/views/privacypolicy/index.html'});

	        $routeProvider.when('/' ,{title: 'Home',templateUrl: '/js/app/views/home/index.html', controller: 'HomeController'});
 		$routeProvider.when('/blog' ,{title: 'Blog',templateUrl: '/js/app/views/blog/index.html', controller: 'BlogController'});
		$routeProvider.when('/blog/:id' ,{title: 'Blog',templateUrl: '/js/app/views/blog/index.html', controller: 'BlogController'});
	        $routeProvider.when('/404', {title: 'Page Not Found',templateUrl: '/js/app/views/404/index.html'});
	        $routeProvider.otherwise({redirectTo:'/404'});
            $locationProvider.html5Mode(true);
            Stripe.setPublishableKey('pk_test_Vn1IqrbVrEeHzecWCrj36rS7');
       }
    ])
    .value('$anchorScroll', angular.noop)
    .value('debug',1)
    .value('siteUrl','/')
    .value('settings',{apiKey: '1a314ce4c164818df42a1721221d35484c6ebb85198e856840210de1dba2788d', url: 'http://api2.justicerobles.com/', siteId: 'mousebit'})

    .controller('Ctrl', function() {
        }
    );

var Ctrl = angular.module('Controllers',[]);

Ctrl.controller('BlogController',['$scope','API','Variables',
    function($scope, API, Variables) {
		 API.postRequest({'request':'blogs','action':'list'})
	                .then(function(d) {
        	                console.log(d);
        	                $scope.blogs = d.data;
                });

}]);

Ctrl.controller('FooterController',['$scope',function($scope) {

}]);

Ctrl.controller('HeaderController',['$scope','$cookies', function($scope, $cookies) {
	$scope.log = function(msg) {
		console.log(msg);
	}
}]);

Ctrl.controller('HomeController',['$scope','API','Variables', 'Quotes',
    function($scope, API, Variables, Quotes) {
    $scope.submitted = 0;
    $scope.contactSubmitted = 0;
	$scope.sendRequest = function() {
		$scope.apiRequest = ["list"];
		$scope.apiResponse = API.apiRequest(["list"],"users?action=list");
	};
    $scope.submitQuotes = function() {
        $scope.res =  Quotes.add($scope.name,$scope.phone,$scope.email,$scope.company,$scope.serviceType,$scope.comments)
            .then(function(d) {
                $scope.submitted = 1;
                return d;
            });

    };
    $scope.submitContact = function() {
        API.apiRequest(['new',$scope.name,$scope.email,$scope.company,$scope.message],'contact&action=new&name='+$scope.name+'&email='+$scope.email+'&company='+$scope.company+'&message='+$scope.message)
            .then(function(d) {
                $scope.contactSubmitted = 1;
            });
    };
}]);

app.controller('LoginController',['$scope', 'API', 'Auth', '$rootScope', '$cookies',
    function($scope, API, Auth,$rootScope,$cookies) {
	$scope.showForm = 1;
	$scope.exit = function() {
		$scope.showForm = 1;
	}
	$scope.logIn = function() {
		$scope.showForm = 0;
		$scope.login =  Auth.login($scope.email,$scope.password);
        	$cookies.name = "Test";
		if ($scope.login) {
			$scope.showForm = 1;
		}
/*		if ($scope.email == '') {
			console.error("Email not set");
			return false;
		}		
		if ($scope.password == '') {
			console.error("Password not set");
			return false;
		}
		console.log($scope.email);
		console.log($scope.password);
		$scope.password = CryptoJS.SHA256($scope.password);		
		$scope.loginResponse = API.apiRequest(["login",$scope.email,$scope.password],"users?action=login&user="+$scope.email+"&password="+$scope.password);
        console.log($scope.loginResponse.reason_code);
        console.log($scope.loginResponse.siteId);
        console.log($scope.loginResponse.reason_text);
        console.log($scope.loginResponse);
        console.log($rootScope.response);
		if ($scope.loginResponse.reason_code != 1) {
      //      $scope.showForm = 1;
        } else {
            // TODO: Set cookies and forward to homepage
        }
*/
	};	
}]);

Ctrl.controller('NavController',['$scope','$cookies','Variables', function($scope, $cookies,Variables) {
	$scope.log = function(msg) {
		console.log(msg);
	}

	$scope.showPortfolio = Variables.showPortfolio;
	console.log(Variables.showPortfolio);
    if (typeof $cookies !== 'undefined') {
        $scope.name = $cookies.name;
    }
    
   $scope.updateHero = function() {
        console.log('test');
        console.log();
        if (!$('.Portfolio').hasClass('ng-scope') && $('#content > div > div > img')) {
        $('#bs-example-navbar-collapse-1 > ul > li:nth-child(2)').css('pointer-events','none');
            $( "#content > div > div > img" ).animate({
                    opacity: 0
                  }, 2000, function() {
                    // Animation complete.
                    $(".Portfolio").css('display','block');
                    $('#content > div > div > img').css('display','none');
                    $('#bs-example-navbar-collapse-1 > ul > li:nth-child(2)').css('pointer-events','all');

                    });

            
        } else {
            $('#bs-example-navbar-collapse-1 > ul > li:nth-child(2)').css('pointer-events','none');
            $('#content > div > div > img').css('margin-top',"0px").css('display','block');
            $('#content > div > div > img').animate({
            opacity:1
            },1000,function() {
                $('#bs-example-navbar-collapse-1 > ul > li:nth-child(2)').css('pointer-events','all');
            });
            
        }
    }
}]);

app.controller('PaymentController',['$scope','API', function($scope,API) {
        $scope.response = '';
        $scope.handler = function(status,response) {
            $scope.response = status
            console.log(status, response);
            API.apiRequest(['pay',response.id,100],"payment&action=pay&stripeToken="+response.id+"&amount=100");
        };

        $scope.submit = function() {
            Stripe.card.createToken({
                number: $scope.card.number,
                cvc: $scope.card.cvc,
                exp_month: $scope.card.exp_month,
                exp_year: $scope.card.exp_year,
                amount: 20
            },$scope.handler);
        };
    }]);

app.controller('PortfoliosController',['$scope', 'API',  '$rootScope', '$cookies',
    function($scope, API,$rootScope,$cookies) {
    console.log('loaded');
	API.postRequest({'request':'portfolios','action':'list'})
		.then(function(d) {
			console.log(d);
			$scope.portfolios = d.data;
		});
}]);

app.controller('QuotesController',['$scope', 'API', 'Quotes', '$rootScope', '$cookies',
    function($scope, API, Quotes,$rootScope,$cookies) {
	$scope.showForm = 1;
	$scope.exit = function() {
		$scope.showForm = 1;
	}
	$scope.add = function() {
		$scope.showForm = 0;
		$scope.res =  Quotes.add($scope.name,$scope.phone,$scope.email,$scope.company,$scope.serviceType,$scope.comments);
 
		if ($scope.res) {
			$scope.showForm = 1;
		}
	};	
}]);

app.controller('ServicesController',['$scope','$routeParams', function($scope, $routeParams) {
//body
	$scope.action = $routeParams.service;
}]);

app.factory('Auth',['API','$cookies','debug','$location','$route','siteUrl',
    function(API,$cookies,debug,$location,$route,siteUrl) {
        return {
            updateUser: function(obj) {
                if (obj.password != '') {
                    obj.password = CryptoJS.SHA256(obj.password);
                    var result = API.apiRequest(['updateUser', obj.id,obj.firstName,obj.lastName,obj.email,obj.isActive,obj.password], "users&action=updateUser&userId=" + id+"&firstName="+obj.firstName+"&lastName="+obj.lastName+"&email="+obj.email+"&isActive="+obj.isActive+"&password="+obj.password)
                        .then(function (d) {
                            return d;
                        });
                }    else {
                    var result = API.apiRequest(['updateUser', obj.id,obj.firstName,obj.lastName,obj.email,obj.isActive], "users&action=updateUser&userId=" + id+"&firstName="+obj.firstName+"&lastName="+obj.lastName+"&email="+obj.email+"&isActive="+obj.isActive)
                        .then(function (d) {
                            return d;
                        });
                }
                return result;
            },
            getUser: function(id) {
                var result = API.apiRequest(['getUser',id],"users&action=getUser&userId="+id)
                    .then(function(d) {
                        return d;
                    });
                return result;
            },
            delete: function(id) {
                var result = API.apiRequest(['delete',id],"users&action=delete&userId="+id)
                    .then(function(d) {
                        return d;
                    });
                return result;
            },
            listUsers: function() {
                var result = API.apiRequest(['list'],"users&action=list")
                    .then(function(d) {
                        return d;
                    });
                return result;
            },
            checkToken: function() {
                var result = API.apiRequest(['checkToken'],"users&action=checkToken").
                    then(function(d) {
                        return d;
                    });
                return result;
            },
            login: function(email,password) {
                if (email == '') {
                    console.error("Email not set");
                    return false;
                }
                if (password == '') {
                    console.error("Password not set");
                    return false;
                }
                password = CryptoJS.SHA256(password);
                API.apiRequest(["login",email,password],"users&action=login&user="+email+"&password="+password).
                    then(function(d) {
                        if (debug)
                            console.log(d);

                        if (d.reason_code != 1) {
                            //  $scope.showForm = 1;
                            return false;
                        } else {
                            // TODO: Set cookies and forward to homepage

                            $cookies.token = d.data.sessionToken;
                            window.location.href = siteUrl;
                        }
                    });
                return true;
            },
            logout: function(){
                API.apiRequest(['getUser',$cookies.token],"users&action=getUser&stoken="+$cookies.token)
                    .then(function(d) {
                        API.apiRequest(['track', $cookies.token,'LOGOUT','User '+ d.data.email+' has logged out'],
                            "tracking&action=track&stoken="+ $cookies.token+"&type=LOGOUT&tAction=User "+ d.data.email+" has logged out").then(function(d) {
                                delete $cookies.token;
                                window.location.href = siteUrl;
                            });
                    });



            }
        };
    }]);

app.factory('Quotes',['API',function(API) {
    return {
        add: function(name,phone,email,company,serviceType,comments) {
                var loginResponse = API.apiRequest(["new",name,phone,email,company,serviceType,comments],"quotes&action=new&name="+name+"&phone="+phone+"&email="+email+"&company="+company+"&serviceType="+serviceType+"&comments="+comments)
                    .then(function(d) {
                      return d;
                    });
            return loginResponse;
	}
    };
}]);

app.service('Variables',function() {
	var service = {showPortfolio:0};



	
	return service;
});

app.service('API',['$http', '$cookies','settings', function($http, $cookies, settings) {
    var service = {};
    service.url = settings.url;
    service.siteId = settings.siteId;
    service.key = settings.apiKey;

    service.apiRequest =  function(params,request) {
        // TODO Add resource code
        var tstr = '';
        if (typeof $cookies.token !== 'undefined') {
            params.push($cookies.token);
            tstr = "&stoken="+$cookies.token;
        }
        params.sort();
        params.push(service.key);
        params = params.join('');
        var token = tstr+"&siteId=" + service.siteId + "&token=" + CryptoJS.SHA256(params);
        var promise = $http({
            method: 'GET',
            cache: true,
            url: service.url + request + token
        }).
            error(function(data, status) {
                console.error(data.reason_text);
            })
            .then(function(response) {
                return response.data;
            });
        return promise;
    };

    var param = function(obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for(name in obj) {
            value = obj[name];

            if(value instanceof Array) {
                for(i=0; i<value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if(value instanceof Object) {
                for(subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if(value !== undefined && value !== null)
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };
    $http.defaults.transformRequest = [function(data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];

    service.postRequest =  function(params) {
        // TODO Add resource code
        var arr = [];

        var tstr = '';
        if (typeof $cookies.token !== 'undefined') {
            params.stoken = $cookies.token;
            tstr = "&stoken="+$cookies.token;
        }
        for (var key in params) {
            if (key != 'request')
                arr.push(params[key]);
        }
        arr.sort();
        arr.push(service.key);
        arr = arr.join('');
        params.siteId = service.siteId;
        params.token = ""+CryptoJS.SHA256(arr);
        console.log(params);
        var promise = $http({
            method: 'POST',
            url: service.url,
            data:params,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
            error(function(data, status) {
                console.error(data.reason_text);
            })
            .then(function(response) {
                return response.data;
            });
        return promise;
    };

    return service;
}]);
