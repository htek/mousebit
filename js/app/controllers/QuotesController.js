app.controller('QuotesController',['$scope', 'API', 'Quotes', '$rootScope', '$cookies',
    function($scope, API, Quotes,$rootScope,$cookies) {
	$scope.showForm = 1;
	$scope.exit = function() {
		$scope.showForm = 1;
	}
	$scope.add = function() {
		$scope.showForm = 0;
		$scope.res =  Quotes.add($scope.name,$scope.phone,$scope.email,$scope.company,$scope.serviceType,$scope.comments);
 
		if ($scope.res) {
			$scope.showForm = 1;
		}
	};	
}]);
