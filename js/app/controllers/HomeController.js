Ctrl.controller('HomeController',['$scope','API','Variables', 'Quotes',
    function($scope, API, Variables, Quotes) {
    $scope.submitted = 0;
    $scope.contactSubmitted = 0;
	$scope.sendRequest = function() {
		$scope.apiRequest = ["list"];
		$scope.apiResponse = API.apiRequest(["list"],"users?action=list");
	};
    $scope.submitQuotes = function() {
        $scope.res =  Quotes.add($scope.name,$scope.phone,$scope.email,$scope.company,$scope.serviceType,$scope.comments)
            .then(function(d) {
                $scope.submitted = 1;
                return d;
            });

    };
    $scope.submitContact = function() {
        API.apiRequest(['new',$scope.name,$scope.email,$scope.company,$scope.message],'contact&action=new&name='+$scope.name+'&email='+$scope.email+'&company='+$scope.company+'&message='+$scope.message)
            .then(function(d) {
                $scope.contactSubmitted = 1;
            });
    };
}]);
