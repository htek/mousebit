app.controller('PaymentController',['$scope','API', function($scope,API) {
        $scope.response = '';
        $scope.handler = function(status,response) {
            $scope.response = status
            console.log(status, response);
            API.apiRequest(['pay',response.id,100],"payment&action=pay&stripeToken="+response.id+"&amount=100");
        };

        $scope.submit = function() {
            Stripe.card.createToken({
                number: $scope.card.number,
                cvc: $scope.card.cvc,
                exp_month: $scope.card.exp_month,
                exp_year: $scope.card.exp_year,
                amount: 20
            },$scope.handler);
        };
    }]);
