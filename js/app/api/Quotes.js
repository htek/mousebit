app.factory('Quotes',['API',function(API) {
    return {
        add: function(name,phone,email,company,serviceType,comments) {
                var loginResponse = API.apiRequest(["new",name,phone,email,company,serviceType,comments],"quotes&action=new&name="+name+"&phone="+phone+"&email="+email+"&company="+company+"&serviceType="+serviceType+"&comments="+comments)
                    .then(function(d) {
                      return d;
                    });
            return loginResponse;
	}
    };
}]);
