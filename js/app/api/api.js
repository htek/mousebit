app.service('API',['$http', '$cookies','settings', function($http, $cookies, settings) {
    var service = {};
    service.url = settings.url;
    service.siteId = settings.siteId;
    service.key = settings.apiKey;

    service.apiRequest =  function(params,request) {
        // TODO Add resource code
        var tstr = '';
        if (typeof $cookies.token !== 'undefined') {
            params.push($cookies.token);
            tstr = "&stoken="+$cookies.token;
        }
        params.sort();
        params.push(service.key);
        params = params.join('');
        var token = tstr+"&siteId=" + service.siteId + "&token=" + CryptoJS.SHA256(params);
        var promise = $http({
            method: 'GET',
            cache: true,
            url: service.url + request + token
        }).
            error(function(data, status) {
                console.error(data.reason_text);
            })
            .then(function(response) {
                return response.data;
            });
        return promise;
    };

    var param = function(obj) {
        var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

        for(name in obj) {
            value = obj[name];

            if(value instanceof Array) {
                for(i=0; i<value.length; ++i) {
                    subValue = value[i];
                    fullSubName = name + '[' + i + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if(value instanceof Object) {
                for(subName in value) {
                    subValue = value[subName];
                    fullSubName = name + '[' + subName + ']';
                    innerObj = {};
                    innerObj[fullSubName] = subValue;
                    query += param(innerObj) + '&';
                }
            }
            else if(value !== undefined && value !== null)
                query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
        }

        return query.length ? query.substr(0, query.length - 1) : query;
    };
    $http.defaults.transformRequest = [function(data) {
        return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
    }];

    service.postRequest =  function(params) {
        // TODO Add resource code
        var arr = [];

        var tstr = '';
        if (typeof $cookies.token !== 'undefined') {
            params.stoken = $cookies.token;
            tstr = "&stoken="+$cookies.token;
        }
        for (var key in params) {
            if (key != 'request')
                arr.push(params[key]);
        }
        arr.sort();
        arr.push(service.key);
        arr = arr.join('');
        params.siteId = service.siteId;
        params.token = ""+CryptoJS.SHA256(arr);
        console.log(params);
        var promise = $http({
            method: 'POST',
            url: service.url,
            data:params,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
            error(function(data, status) {
                console.error(data.reason_text);
            })
            .then(function(response) {
                return response.data;
            });
        return promise;
    };

    return service;
}]);
