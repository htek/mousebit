app.factory('Auth',['API','$cookies','debug','$location','$route','siteUrl',
    function(API,$cookies,debug,$location,$route,siteUrl) {
        return {
            updateUser: function(obj) {
                if (obj.password != '') {
                    obj.password = CryptoJS.SHA256(obj.password);
                    var result = API.apiRequest(['updateUser', obj.id,obj.firstName,obj.lastName,obj.email,obj.isActive,obj.password], "users&action=updateUser&userId=" + id+"&firstName="+obj.firstName+"&lastName="+obj.lastName+"&email="+obj.email+"&isActive="+obj.isActive+"&password="+obj.password)
                        .then(function (d) {
                            return d;
                        });
                }    else {
                    var result = API.apiRequest(['updateUser', obj.id,obj.firstName,obj.lastName,obj.email,obj.isActive], "users&action=updateUser&userId=" + id+"&firstName="+obj.firstName+"&lastName="+obj.lastName+"&email="+obj.email+"&isActive="+obj.isActive)
                        .then(function (d) {
                            return d;
                        });
                }
                return result;
            },
            getUser: function(id) {
                var result = API.apiRequest(['getUser',id],"users&action=getUser&userId="+id)
                    .then(function(d) {
                        return d;
                    });
                return result;
            },
            delete: function(id) {
                var result = API.apiRequest(['delete',id],"users&action=delete&userId="+id)
                    .then(function(d) {
                        return d;
                    });
                return result;
            },
            listUsers: function() {
                var result = API.apiRequest(['list'],"users&action=list")
                    .then(function(d) {
                        return d;
                    });
                return result;
            },
            checkToken: function() {
                var result = API.apiRequest(['checkToken'],"users&action=checkToken").
                    then(function(d) {
                        return d;
                    });
                return result;
            },
            login: function(email,password) {
                if (email == '') {
                    console.error("Email not set");
                    return false;
                }
                if (password == '') {
                    console.error("Password not set");
                    return false;
                }
                password = CryptoJS.SHA256(password);
                API.apiRequest(["login",email,password],"users&action=login&user="+email+"&password="+password).
                    then(function(d) {
                        if (debug)
                            console.log(d);

                        if (d.reason_code != 1) {
                            //  $scope.showForm = 1;
                            return false;
                        } else {
                            // TODO: Set cookies and forward to homepage

                            $cookies.token = d.data.sessionToken;
                            window.location.href = siteUrl;
                        }
                    });
                return true;
            },
            logout: function(){
                API.apiRequest(['getUser',$cookies.token],"users&action=getUser&stoken="+$cookies.token)
                    .then(function(d) {
                        API.apiRequest(['track', $cookies.token,'LOGOUT','User '+ d.data.email+' has logged out'],
                            "tracking&action=track&stoken="+ $cookies.token+"&type=LOGOUT&tAction=User "+ d.data.email+" has logged out").then(function(d) {
                                delete $cookies.token;
                                window.location.href = siteUrl;
                            });
                    });



            }
        };
    }]);
