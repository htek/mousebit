/**
 * Created by Tidus on 9/12/14.
 */
'use strict';
var app = angular.module('Mousebit', [
        "ui.router",
	    'Controllers',
        "ui.bootstrap",
        "ngResource",
        'ngRoute',
        "ngAnimate",
	    "ngCookies",
        "ngSanitize"
    ])
 .run ( ['$rootScope','$location', '$routeParams', function($rootScope, $location, $routeParams) {

	$rootScope.$on('$routeChangeSuccess', function(e, next, cur) {
	console.log(cur);
       if ($location.path() == '/' && typeof(cur) !== 'undefined') {
		console.log("what");
		window.location.href = '/';
       }
    });
}])

 .config( ['$routeProvider', '$locationProvider', '$resourceProvider'
,        function ($routeProvider,  $locationProvider, $resourceProvider) {
            $routeProvider.when('/login', {title: 'login',templateUrl: '/js/app/views/login/index.html', controller: 'LoginController'});
            $routeProvider.when('/new-quote', {title: 'new-quote',templateUrl: '/js/app/views/quotes/index.html', controller: 'QuotesController'});
            $routeProvider.when('/about-us', {title: 'About Us',templateUrl: '/js/app/views/aboutUs/index.html'});
            $routeProvider.when('/portfolios', {title: 'Portfolios',templateUrl: '/js/app/views/portfolio/index.html', controller: 'PortfoliosController'});
	        $routeProvider.when('/services/:service', {title: 'Services',templateUrl: '/js/app/views/services/index.html', controller: 'ServicesController'});
            $routeProvider.when('/payment', {title: 'Payment',templateUrl: '/js/app/views/payment/index.html', controller: 'PaymentController'});
		$routeProvider.when('/privacy-policy', {title: 'Privacy Policy',templateUrl: '/js/app/views/privacypolicy/index.html'});

	        $routeProvider.when('/' ,{title: 'Home',templateUrl: '/js/app/views/home/index.html', controller: 'HomeController'});
 		$routeProvider.when('/blog' ,{title: 'Blog',templateUrl: '/js/app/views/blog/index.html', controller: 'BlogController'});
		$routeProvider.when('/blog/:id' ,{title: 'Blog',templateUrl: '/js/app/views/blog/index.html', controller: 'BlogController'});
	        $routeProvider.when('/404', {title: 'Page Not Found',templateUrl: '/js/app/views/404/index.html'});
	        $routeProvider.otherwise({redirectTo:'/404'});
            $locationProvider.html5Mode(true);
            Stripe.setPublishableKey('pk_test_Vn1IqrbVrEeHzecWCrj36rS7');
       }
    ])
    .value('$anchorScroll', angular.noop)
    .value('debug',1)
    .value('siteUrl','/')
    .value('settings',{apiKey: '1a314ce4c164818df42a1721221d35484c6ebb85198e856840210de1dba2788d', url: 'http://api2.justicerobles.com/', siteId: 'mousebit'})

    .controller('Ctrl', function() {
        }
    );

var Ctrl = angular.module('Controllers',[]);
