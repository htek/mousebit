<h1>Memberlist</h1> 
<form action="memberlist" method="post">
<table> 
    <tr> 
        <th>ID</th> 
        <th>Username</th> 
        <th>E-Mail Address</th>
        <th>Admin</th>
        <th>delete</th> 
    </tr> 
    {foreach from=$rows item=row}
        <tr> 
            <td>{$row.id}</td> <!-- htmlentities is not needed here because $row['id'] is always an integer --> 
            <td>{$row.username}</td> 
            <td>{$row.email}</td> 
            <td><input type="checkbox" name="admin{$row.id}" min="0" max="2"{if $row.admin eq '1'} checked=checked{/if}></td>
            <td><input type="checkbox" name="delete{$row.id}" min="0" max="2"{if $row.delete eq '1'} checked=checked{/if}></td>
        </tr> 
    {/foreach}
</table> 
<br />
<input name="submit" type="submit"></form>
<br /><br />
</form>
<a href="private">Go Back</a><br />
